#!/bin/bash

files=(
	/usr/local/bin/scripts
	/home/svetislav/.bash_aliases
	/home/svetislav/.bash_profile
	/home/svetislav/.bashrc
	/home/svetislav/.gitconfig
	/home/svetislav/.profile
	/home/svetislav/.config
	/home/svetislav/.ssh
	/etc/anacrontab
)

if [ $# -eq 0 ]; then
	echo "#########################################################################################################################"
	echo "First argument should be backup folder name/path (name folder using single word)."
	echo "All other arguments should be paths to additional files and folders to be backed up."
	echo "Files and folders backed up by default:"
	printf '\t%s\n' "${files[@]}"
	echo "File 'log.txt' inside backup folder holds all the informations about backed up files and folders."
	echo "In case of error (i.e. file or folder to be backed up is missing), error informations will be stored in 'error.txt' file."
	echo "#########################################################################################################################"
else
	folder=$1
	dateCreated=$(date)

	if [ ! -d $folder ]; then
		mkdir -p $folder

		echo "Date created: $dateCreated" >> $folder/log.txt
		echo >> $folder/log.txt
		echo "Files and folders copied to '$folder':" | tee -a $folder/log.txt

		for file in ${files[@]}
	    do
	    	cp -r $file $folder 2>> $folder/error.txt
	    	if [ $? -eq 0 ]; then
		    	echo "	$file" | tee -a $folder/log.txt
			fi
	    done

		if [ $# -gt 1 ]; then
			missingFiles=()
			shift # we skip first argument so we can loop through the rest of the arguments in the for loop
			for file in "$@"
			do
				if [ -f $file -o -d $file ]; then
					cp -r $file $folder
			    	echo "	$file" | tee -a $folder/log.txt
			    else
			    	missingFiles+=($file)
				fi
			done
			if [ ${#missingFiles[@]} -ne 0 ]; then
			    echo | tee -a $folder/error.txt
			    echo "Files and folders not copied to '$folder':" | tee -a $folder/error.txt
			    for missingFile in ${missingFiles[@]}
			    do
			    	echo "	$missingFile (doesn't exist)" | tee -a $folder/error.txt
			    done
			fi
		fi
		echo "Check '$folder/error.txt' to ensure all files and folders are copied!"
	else
		echo "Folder '$folder' already exists. Aborting!"
	fi
fi
