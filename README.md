# backupFiles

> `backupFiles` is a simple Bash script that backs up important files and folders.

First argument passed to a script should be backup folder name/path (name folder using single word).

All other arguments should be paths to additional files and folders to be backed up.

File 'log.txt' inside backup folder holds all the informations about backed up files and folders.

In case of error (i.e. file or folder to be backed up is missing), error informations will be stored in 'error.txt' file.

# backupFromFile

> `backupFromFile` is a simple Bash script that backs up important files and folders that are listed in a file.

First argument passed to a script should be backup folder name/path (name folder using single word).

Second argument should be a file that contains a list of files and folders to be backed up.

File 'log.txt' inside backup folder holds all the informations about backed up files and folders.

In case of error (i.e. file or folder to be backed up is missing), error informations will be stored in 'error.txt' file.

# backupAutomatic

> `backupAutomatic` is a simple Bash script that uses anacron to automatically back up files and folders daily. It should not be used manually.

The script takes three arguments. The first one is a destination in which files and folders are backed up.

The second one is a file that lists all files and folders to be backed up.

The third one is a maximum number of archives that are created in backup folder.

Backed up files and folders are stored in a list in '~/Storage/Razno/Backup/filesToBackup.txt'.

File 'log.txt' inside backup folder holds all the informations about backed up files and folders.

In case of error (i.e. file or folder to be backed up is missing), error informations will be stored in 'error.txt' file.
