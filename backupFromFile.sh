#!/bin/bash

if [ $# -eq 0 ]; then
	echo "#########################################################################################################################"
	echo "First argument should be backup folder name/path (name folder using single word)."
	echo "Second argument should be a file that contains a list of files and folders to be backed up."
	echo "File 'log.txt' inside backup folder holds all the informations about backed up files and folders."
	echo "In case of error (i.e. file or folder to be backed up is missing), error informations will be stored in 'error.txt' file."
	echo "#########################################################################################################################"
else
	if [ $# -eq 2 ]; then
		folder=$1
		list=$2
		missingFiles=()
		dateCreated=$(date)

		if [ ! -d $folder -a -f $list ]; then
			mkdir -p $folder

			echo "Date created: $dateCreated" >> $folder/log.txt
			echo >> $folder/log.txt
			echo "Files and folders copied to '$folder':" | tee -a $folder/log.txt

			while read -r listItem
			do
				if [ -f $listItem -o -d $listItem ]; then
					cp -r $listItem $folder 2>> $folder/error.txt
			    	echo "	$listItem" | tee -a $folder/log.txt
			    else
			    	missingFiles+=($listItem)
				fi
			done < "$list"

			if [ ${#missingFiles[@]} -ne 0 ]; then
			    echo "Files and folders not copied to '$folder':" | tee -a $folder/error.txt
			    for missingFile in ${missingFiles[@]}
			    do
			    	echo "	$missingFile (doesn't exist)" | tee -a $folder/error.txt
			    done
			fi
			echo "Check '$folder/error.txt' to ensure all files and folders are copied!"
		else
			echo "Either '$folder' already exists or list of files and folders to backup that you provided doesn't exist. Aborting!"
		fi
	else
		if [ $# -eq 1 ]; then
			echo "List of files and folders to backup is missing. Aborting!"
		elif [ $# -gt 2 ]; then
			echo "Too many arguments. Aborting!"
		fi
	fi
fi
