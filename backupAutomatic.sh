#!/bin/bash

if [ $# -eq 0 ]; then
	echo "#########################################################################################################################"
	echo "This is a script that is run automatically by Cron (/etc/anacrontab) and it should not be used manually."
	echo "The script takes three arguments. The first one is a destination in which files and folders are backed up."
	echo "The second one is a file that lists all files and folders to be backed up."
	echo "The third one is a maximum number of archives that are created in backup folder."
	echo "Backed up files and folders are stored in a list in '~/Storage/Razno/Backup/filesToBackup.txt'."
	echo "File 'log.txt' inside backup folder holds all the informations about backed up files and folders."
	echo "In case of error (i.e. file or folder to be backed up is missing), error informations will be stored in 'error.txt' file."
	echo "#########################################################################################################################"
elif [ $# -eq 3 ]; then

	folder=$1
	list=$2
	maxArchivesNo=$3
	dateCreated=$(date +%Y-%b-%d-%Hh%Mm%Ss)
	backupFolder="backup-$dateCreated"

	if [ ! -d $folder ]; then
		mkdir -p $folder
	fi
	
	mkdir $folder/$backupFolder

	echo "Files and folders copied to '$backupFolder':" >> $folder/$backupFolder/log.txt

	while read -r listItem
	do
		cp -r $listItem $folder/$backupFolder 2>> $folder/$backupFolder/error.txt
    	if [ $? -eq 0 ]; then
	    	echo "	$listItem" >> $folder/$backupFolder/log.txt
		fi
	done < "$list"

    tar -zcf $folder/$backupFolder.tar.gz -C $folder/ $backupFolder
    rm -r $folder/$backupFolder

    # Keep only 10 newest backup archives
	numberOfFiles=`find $folder/backup-*tar.gz -type f | wc -l`

	if [ $numberOfFiles -gt $maxArchivesNo ]; then
		max=$((maxArchivesNo + 1))
		ls -1t $folder/backup-*tar.gz | tail -n +$max | xargs rm -f
	fi
fi
